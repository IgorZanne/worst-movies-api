﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Worst.Entity;

namespace Worst.Business
{
    public class ReadCsvFile
    {
        public List<Movie> Execute(string fileName)
        {
            var lines = new List<Movie>();

            var config = GetCsvConfiguration();

            using (var reader = new StreamReader(fileName))
            using (var csv = new CsvReader(reader, config))
            {
                var records = csv.GetRecords<Movie>();
                if (records != null && records.Any())
                    lines = records.ToList();
            }

            return lines;
        }

        private CsvConfiguration GetCsvConfiguration()
        {
            return new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
                PrepareHeaderForMatch = args => args.Header.ToLower(),
                Delimiter = ";"
            };
        }
    }
}
