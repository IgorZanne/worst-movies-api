﻿using System.Collections.Generic;
using System.Linq;
using Worst.Data;
using Worst.Entity;

namespace Worst.Business
{
    public class AwardIntervalBo
    {
        private readonly List<ProducerAward> producers;

        public AwardIntervalBo(ApiContext context)
        {
            producers = GetProducers(context);
        }

        public List<ProducerAward> GetLongerIntervalProducers()
        {
            if (producers == null || !producers.Any()) return new List<ProducerAward>();

            var maxInterval = producers.Max(e => e.Interval);
            return producers.Where(e => e.Interval == maxInterval).ToList();
        }

        public List<ProducerAward> GetShorterIntervalProducers()
        {
            if (producers == null || !producers.Any()) return new List<ProducerAward>();

            var minInterval = producers.Min(e => e.Interval);
            return producers.Where(e => e.Interval == minInterval).ToList();
        }

        private List<ProducerAward> GetProducers(ApiContext context)
        {
            var producersWithMoreThanOneAward = GetProducersWithMoreThanOneAward(context);

            if (producersWithMoreThanOneAward == null || !producersWithMoreThanOneAward.Any())
                return new List<ProducerAward>();

            var producersArwards = new List<ProducerAward>();
            foreach (var producer in producersWithMoreThanOneAward)
            {
                for (int i = 1; i < producer.Awards.Count; i++)
                {
                    var previous = producer.Awards[i - 1];
                    var current = producer.Awards[i];
                    var producerAward = new ProducerAward
                    {
                        Producer = producer.Producer,
                        PreviousWin = previous,
                        FollowingWin = current,
                        Interval = current - previous
                    };
                    producersArwards.Add(producerAward);
                }
            }

            return producersArwards;
        }

        private List<ProducerInfo> GetProducersWithMoreThanOneAward(ApiContext context)
        {
            var awardWinningMovies = context.Movies
                .Where(e => !string.IsNullOrEmpty(e.Winner)
                         && e.Winner.ToUpper().Equals("YES"))
                .OrderBy(e => e.Year)
                .ToList();

            return awardWinningMovies
                .GroupBy(e => new { e.Producer })
                .Where(e => e.Count() > 1)
                .Select(e => new ProducerInfo
                {
                    Producer = e.Key.Producer,
                    Awards = e.Select(a => a.Year).ToList()
                }).ToList();
        }
    }
}
