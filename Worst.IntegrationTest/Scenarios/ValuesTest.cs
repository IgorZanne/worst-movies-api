﻿using System.Net;
using FluentAssertions;
using System.Threading.Tasks;
using Xunit;
using Worst.IntegrationTests.Fixtures;
using Microsoft.Extensions.Configuration;

namespace Worst.IntegrationTests.Scenarios
{
    public class ValuesTest
    {
        private readonly TestContext _testContext;
        public ValuesTest()
        {
            _testContext = new TestContext();
        }

        [Fact]
        public async Task Values_Get_ReturnsOkResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/awardinterval");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Values_CorrectContentType()
        {
            var response = await _testContext.Client.GetAsync("/api/awardinterval");
            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.ToString().Should().Be("application/json; charset=utf-8");
        }

        [Fact]
        public async Task Values_GetById_ReturnsBadRequestResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/awardinterval/XXX");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}
