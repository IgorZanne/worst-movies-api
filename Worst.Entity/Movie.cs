﻿namespace Worst.Entity
{
    public class Movie
    {
        public int Year { get; set; }
        public string Title { get; set; }
        public string Studios { get; set; }
        public string Producers { get; set; }
        public string Winner { get; set; }
    }
}
