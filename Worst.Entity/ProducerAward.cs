﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worst.Entity
{
    public class ProducerAward
    {
        public string Producer { get; set; }
        public int Interval { get; set; }
        public int PreviousWin { get; set; }
        public int FollowingWin { get; set; }
    }
}
