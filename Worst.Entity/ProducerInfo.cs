﻿using System.Collections.Generic;

namespace Worst.Entity
{
    public class ProducerInfo
    {
        public ProducerInfo()
        {
            this.Awards = new List<int>();
        }

        public string Producer { get; set; }
        public List<int> Awards { get; set; }
    }
}
