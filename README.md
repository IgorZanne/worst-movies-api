# README #

Este documento tem por finalidade mostrar como deve ser a execução e como é o funcionamento da API de leitura de indicados e vencedores da categoria de Piores Filmes do Golden Raspberry Awards.
Como funcinamento primordial, a API realiza a leitura de um arquivo csv que contem a base inicial para os indicados e vencedores de piores filmes. E como método único e principal, é realizado a consulta dos produtores de filmes que possuem mais de um prêmio com período mais curto entre as premiações e período mais longo entre duas premiações.

### Como configurar a aplicação para rodar locamente? ###

* Faça clone/download do projeto
* Dentro do projeto Worst.WebApi, altere o arquivo appsettings.json, indicando o local do arquivo csv que contem a base de dados inicial na propriedade "CsvDataPath"
* O arquivo csv, deve ter o seguinte formato com header "year;title;studios;producers;winner"
* Execute o projeto acessando Debug > Start Debugging

### Como rodar os testes? ###

* Acesse o menu Test > Run All Tests do Visual Studio