﻿using Microsoft.EntityFrameworkCore;

namespace Worst.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options) { }

        public DbSet<Movie> Movies { get; set; }
    }
}
