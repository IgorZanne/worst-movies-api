﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Worst.Data
{
    public class InitialImport
    {
        private readonly ApiContext context;

        public InitialImport(ApiContext context)
        {
            this.context = context;
        }

        public void Execute(IList<Entity.Movie> movies)
        {
            if (movies == null || !movies.Any()) return;

            var separator = new string[] { ",", " and " };

            foreach (var movie in movies)
            {
                var producers = movie.Producers.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                foreach (var producer in producers)
                {
                    var movieInsert = new Movie
                    {
                        Producer = producer.Trim(),
                        Studios = movie.Studios,
                        Title = movie.Title,
                        Winner = movie.Winner,
                        Year = movie.Year
                    };

                    context.Movies.Add(movieInsert);
                }
            }

            context.SaveChanges();
        }
    }
}
