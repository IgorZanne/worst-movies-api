﻿using System.ComponentModel.DataAnnotations;

namespace Worst.Data
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        public int Year { get; set; }
        public string Title { get; set; }
        public string Studios { get; set; }
        public string Winner { get; set; }
        public string Producer { get; set; }
    }
}
