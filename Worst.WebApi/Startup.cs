using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using Worst.Business;
using Worst.Data;

namespace Worst
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase("worst-movies"));
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Worst Movies API",
                    Version = "v1",
                    Description = "API to reading the list of nominees and winners of the Golden Raspberry Awards' Worst Film category",
                    Contact = new OpenApiContact
                    {
                        Name = "Igor Zanne",
                        Email = "igor.zanne@gmail.com",
                        Url = new Uri("https://github.com/IgorZanne"),
                    },
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Worst Movies API V1");
                c.RoutePrefix = string.Empty;
            });

            var db = serviceProvider.GetRequiredService<ApiContext>();
            var logger = serviceProvider.GetRequiredService<ILogger<Startup>>();

            db.Database.EnsureCreated();

            try
            {
                InitializeDb(db);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred seeding the " +
                    "database with csv file. Error: {Message}", ex.Message);
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void InitializeDb(ApiContext context)
        {
            var fileName = Configuration.GetValue<string>("CsvDataPath");
            var movies = new ReadCsvFile().Execute(fileName);
            new InitialImport(context).Execute(movies);
        }
    }
}
