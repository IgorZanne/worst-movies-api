﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Worst.Business;
using Worst.Data;

namespace Worst.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AwardIntervalController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly ApiContext _context;

        public AwardIntervalController(ApiContext context,
            ILogger<AwardIntervalController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var bo = new AwardIntervalBo(_context);
                var minProducers = bo.GetShorterIntervalProducers();
                var maxProducers = bo.GetLongerIntervalProducers();

                return new JsonResult(new { min = minProducers, max = maxProducers });
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Error ocurred getting award interval. Error: {Message}", e.Message);
                throw;
            }
        }
    }
}
